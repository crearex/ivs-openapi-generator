swagger: '2.0'
info:
  description: REST API for creare-X's virtú IVSPLayer Quiz Backend
  version: 1.0.0
  title: IVSPlayer Quiz REST API
  termsOfService: 'https://www.creare-x.net/terms/'
  contact:
    email: ben@creare-x.net
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
host: localhost
basePath: /quiz/api
tags:
  - name: answer
    description: All the Answers
  - name: question
    description: All the Questions
  - name: tip
    description: All the Tips
schemes:
  - https
  - http
paths:
  /answer:
    get:
      tags:
        - answer
      summary: Get all answers
      description: ''
      operationId: getAllAnswers
      produces:
        - application/json
      parameters: []
      responses:
        '200':
          description: Success
          schema:
            type: array
            items:
              $ref: '#/definitions/AnswerResponse'
  '/answer/{answerId}':
    get:
      tags:
        - answer
      summary: Find answer by id
      description: Returns a single Answer
      operationId: getAnswerById
      produces:
        - application/json
      parameters:
        - name: answerId
          in: path
          required: true
          description: questionId of Answer to return
          type: integer
          format: int32
      responses:
        '200':
          description: Successful Operation
          schema:
            $ref: '#/definitions/AnswerResponse'
        '400':
          description: Invalid id supplied
        '404':
          description: Answer not found
  '/answer/findByQuestion/{questionId}':
    get:
      tags:
        - answer
        - question
      summary: Find answers by questionId
      description: Returns Answers
      operationId: findAnswersByQuestionId
      produces:
        - application/json
      parameters:
        - name: questionId
          in: path
          description: questionId of answer to return
          required: true
          type: integer
          format: int32
      responses:
        '200':
          description: Successful Operation
          schema:
            type: array
            items:
              $ref: '#/definitions/AnswerResponse'
        '400':
          description: Invalid id supplied
        '404':
          description: Answer not found
  /question:
    get:
      tags:
        - question
      summary: Find All Question
      description: ''
      operationId: findAllQuestions
      produces:
        - application/json
      parameters: []
      responses:
        '200':
          description: Success
          schema:
            type: array
            items:
              $ref: '#/definitions/QuestionResponse'
  '/question/{questionId}':
    get:
      tags:
        - question
      summary: Find question by id
      description: Returns a single question
      operationId: getQuestionById
      produces:
        - application/json
      parameters:
        - name: questionId
          in: path
          description: id of question to return
          required: true
          type: integer
          format: int32
      responses:
        '200':
          description: Successful Operation
          schema:
            $ref: '#/definitions/QuestionResponse'
        '400':
          description: Invalid id supplied
        '404':
          description: Question not found
  /tip:
    get:
      tags:
        - tip
      summary: Get all tips
      description: ''
      operationId: getAllTips
      produces:
        - application/json
      parameters: []
      responses:
        '200':
          description: Success
          schema:
            type: array
            items:
              $ref: '#/definitions/TipResponse'
    post:
      tags:
        - tip
      summary: Add a Tip
      description: ''
      operationId: addTip
      parameters:
        - in: body
          name: body
          description: Tip object to create
          required: true
          schema:
            $ref: '#/definitions/CreateTipRequest'
      responses:
        '200':
          description: Success
          schema:
            $ref: '#/definitions/Tip'
        '403':
          description: Not authorized
        '405':
          description: Invalid input
      security:
        - api_key: []
    put:
      tags:
        - tip
      summary: Update a Tip
      description: ''
      operationId: updateTip
      parameters:
        - in: body
          name: body
          description: Tip object to update
          required: true
          schema:
            $ref: '#/definitions/UpdateTipRequest'
      responses:
        '200':
          description: Success
          schema:
            $ref: '#/definitions/TipResponse'
        '403':
          description: Not authorized
        '404':
          description: Not found
        '405':
          description: Invalid input
      security:
        - api_key: []
  '/tip/{tipId}':
    get:
      tags:
        - tip
      summary: Find tip by id
      description: Returns a single tip
      operationId: getTipById
      produces:
        - application/json
      parameters:
        - name: tipId
          in: path
          description: tipId of Tip to return
          required: true
          type: integer
          format: int32
      responses:
        '200':
          description: Successful Operation
          schema:
            $ref: '#/definitions/TipResponse'
        '400':
          description: Invalid id supplied
        '404':
          description: Tip not found
  '/tip/findByQuestion/{questionId}':
    get:
      tags:
        - tip
        - question
      summary: Find tips by questionId
      description: Returns tips
      operationId: findTipsByQuestionId
      produces:
        - application/json
      parameters:
        - name: questionId
          in: path
          description: questionId of tip to return
          required: true
          type: integer
          format: int32
      responses:
        '200':
          description: Successful Operation
          schema:
            type: array
            items:
              $ref: '#/definitions/TipResponse'
        '400':
          description: Invalid id supplied
        '404':
          description: Tip not found
  '/tip/findByAnswer/{answerId}':
    get:
      tags:
        - tip
        - answer
      summary: Find tips by answerId
      description: Returns Tips
      operationId: findTipsByAnswerId
      produces:
        - application/json
      parameters:
        - name: answerId
          in: path
          description: answerId of Tips to return
          required: true
          type: integer
          format: int32
      responses:
        '200':
          description: Successful Operation
          schema:
            type: array
            items:
              $ref: '#/definitions/TipResponse'
        '400':
          description: Invalid id supplied
        '404':
          description: Tip not found
  /mux/sendEvent:
    post:
      summary: Send Event to AWS/MUX
      description: sendEvent
      operationId: sendEvent
      produces:
        - application/json
      parameters:
        - name: answerId
          in: body
          description: answerId which should be sent to the Muxer
          required: true
          schema:
            type: integer
            format: int32
      responses:
        '200':
          description: Success
          schema:
            $ref: '#/definitions/MuxResponse'
        '404':
          description: Question couldn't be found
securityDefinitions:
  api_key:
    type: apiKey
    name: api_key
    in: header
definitions:
  Answer:
    type: object
    properties:
      id:
        type: integer
        format: int32
      question:
        $ref: '#/definitions/Question'
      message:
        type: string
    xml:
      name: Answer
  AnswerResponse:
    type: object
    properties:
      id:
        type: integer
        format: int32
      answer:
        type: string
      questionId:
        type: integer
        format: int32
      question:
        type: string
  CreateAnswerRequest:
    type: object
    properties:
      message:
        type: string
      questionId:
        type: integer
  UpdateAnswerRequest:
    type: object
    properties:
      id:
        type: integer
      message:
        type: string
      questionId:
        type: integer
  Question:
    type: object
    properties:
      id:
        type: integer
        format: int32
      message:
        type: string
    xml:
      name: Question
  QuestionResponse:
    type: object
    properties:
      id:
        type: integer
        format: int32
      question:
        type: string
      answers:
        type: array
        items:
          type: object
          properties:
            answerId:
              type: integer
              format: int32
            answer:
              type: string
  CreateQuestionRequest:
    type: object
    properties:
      id:
        type: integer
      message:
        type: string
      answers:
        type: array
        items:
          type: object
          properties:
            message:
              type: string
  Tip:
    type: object
    properties:
      id:
        type: integer
        format: int32
      question:
        $ref: '#/definitions/Question'
      answer:
        $ref: '#/definitions/Answer'
    xml:
      name: Tip
  TipResponse:
    type: object
    properties:
      id:
        type: integer
        format: int32
      question:
        type: string
      answer:
        type: string
  CreateTipRequest:
    type: object
    properties:
      questionId:
        type: integer
      answerId:
        type: integer
  UpdateTipRequest:
    type: object
    properties:
      tipId:
        type: integer
      questionId:
        type: integer
      answerId:
        type: integer
  MuxResponse:
    type: object
    properties:
      status:
        type: integer
        format: int32
      message:
        type: string
externalDocs:
  description: Find out more about creare-x
  url: 'https://www.creare-x.net'
