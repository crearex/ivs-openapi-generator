# creare-x api-generator Tool

The purpose of this tool to easily generate API Stubs with the openAPI-generator  tool.
The swagger definition is geared towards an interface for AWS IVS Player. 

## Prerequistes

* install packages
  ```bash
  sudo apt install openjdk-16-jdk openjdk-16-doc maven make
  ```

* set JAVA_HOME 
    ```bash
    export JAVA_HOME=/usr/lib/jvm/java-16-openjdk-amd64/
    ```

* install libraries
    ```bash
    make reinstall-libs
    ```

* generate!
    ```bash
    make php
    OR
    make js
    ```

