.PHONY: all

# Paths
OUT = out
LIBPATH = lib
OPENAPI_GENERATOR_PATH = openapi-generator
JARPATH = modules/openapi-generator-cli/target/openapi-generator-cli.jar
SWAGGERFILE = ./swagger.yml

# generator commands
GENERATOR_COMMAND := java -jar ${LIBPATH}/${OPENAPI_GENERATOR_PATH}/${JARPATH}

# LISTS
PHP_CLIENTS = php
PHP_SERVERS := php-slim4 php-laravel php-lumen
JS_CLIENTS := javascript typescript-axios typescript-fetch typescript-jquery
JS_SERVERS := typescript-node

all: php js

# Commands to build specific libraries 
php: php-clients php-servers
php-clients: 
	 @echo "Generating PHP Client-Libraries"; \
		for client in ${PHP_CLIENTS}; do \
			if [ ! -d "./${OUT}/clients/php/$$client;" ]; then \
				mkdir -p ./${OUT}/clients/php/$$client; \
			fi; \
			${GENERATOR_COMMAND} generate -g $$client -i ${SWAGGERFILE} -o ${OUT}/clients/php/$$client; \
		done
php-servers:
	@echo "Generating PHP Server-Libraries"; \
	for server in ${PHP_SERVERS}; do \
		if [ ! -d "${OUT}/servers/php/$$server;" ]; then \
			mkdir -p ./${OUT}/servers/php/$$server; \
		fi; \
		${GENERATOR_COMMAND} generate -g $$server -i ${SWAGGERFILE} -o ${OUT}/servers/php/$$server;  \
	done

js: js-clients js-servers
js-clients:
	@echo "Generating JS Client-Libraries"; \
	for client in ${JS_CLIENTS}; do \
		if [ ! -d "./${OUT}/clients/js/$$client;" ]; then \
			mkdir -p ./${OUT}/clients/js/$$client; \
		fi; \
		${GENERATOR_COMMAND} generate -g $$client -i ${SWAGGERFILE} -o ${OUT}/clients/js/$$client; \
	done

js-servers:
	@echo "Generating JS Server-Libraries"; \
	for server in ${JS_SERVERS}; do \
		if [ ! -d "${OUT}/servers/js/$$server;" ]; then \
			mkdir -p ./${OUT}/servers/js/$$server; \
		fi; \
		${GENERATOR_COMMAND} generate -g $$server -i ${SWAGGERFILE} -o ${OUT}/servers/js/$$server;  \
	done



# clean output directory
clean:
	@rm -rf $(OUT);

# These are to setup the library
clean-lib:
	@echo "Cleaning Lib Path: $(LIBPATH)"; \
		rm -rf $(LIBPATH); \
		mkdir $(LIBPATH);

reinstall-lib: clean-lib generate-libs
	@echo "Done. ";

generate-libs: clone-openapi-generator build-openapi-generator
	@echo "Making the CLI executable ... "
	@chmod +x ${LIBPATH}/${OPENAPI_GENERATOR_PATH}/${JARPATH}
	@echo "Libs installed... \n"

build-openapi-generator: 
	@echo "		Building sources"; \
		cd ${LIBPATH}/${OPENAPI_GENERATOR_PATH}; \
		mvn clean install;

clone-openapi-generator:
	@echo "		Cloning sources ... "; \
		git clone https://github.com/OpenAPITools/openapi-generator.git ${LIBPATH}/${OPENAPI_GENERATOR_PATH}; \
		echo "	Finished cloning. ";
